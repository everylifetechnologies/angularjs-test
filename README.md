# everyLIFE Technologies AngularJS recruitment task

We would like you to write a simple AngularJS (v1) app that fetches data from a [document API](#the-document-api) and display this on a page.

* The page should have 2 tabs for 2 different customers, by switching between tabs, the app would fetch different sets of documents for these 2 customers. For the purposes of this exercise, we are happy if you just hardcode these customers and IDs.
* There should be a checkbox available on the page to show/hide locked documents.
* Locked and unlocked documents should be styled differently.
* Each entry in the list should be of this format: {documentName} [Locked: Yes/No]  -- the last Yes/No bit should be done using an angular filter in html to transform boolean to Yes or No.

We would like you to write tests, preferably using Karma.

## The Document API
The document API would be available at `/api/v1/customers/:customerId/documents` which returns a list of documents for a given customerId in this format:
``` [
  {
    "name": "Document 1",
    "isLocked": false
  },
  {
    "name": "Document 2",
    "isLocked": false
  }
]
```

This api also takes an optional query param 'includeLocked'. If the latter is set to true then the response will include locked documents.
i.e. `/api/v1/customers/123/documents` will return a list of both locked and unlocked documents for customer with id 123